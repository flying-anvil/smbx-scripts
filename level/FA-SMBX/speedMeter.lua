local lib = {}
local better = require('FA-SMBX/betterPrint')

function calculateIndicatorCount(length)
    local speedPercentage = player.speedX / Defines.player_runspeed

    return math.min(math.floor(math.abs(speedPercentage * length)), length)
end

function lib.showSpeedLeftRight(length, column, row)
    length = length or 10
    column = column or 1
    row    = row or 1.5

    local output = ''
    
    local indicatorCount = calculateIndicatorCount(length)

    for i = 1, indicatorCount, 1 do
        if (i == indicatorCount) then
            output = output .. '>'
            goto continue
        end

        output = output .. '='
        ::continue::
    end

    for i = indicatorCount + 1, length, 1 do
        output = output .. '.'
    end

    better.echo(output, column, row)
end

function lib.showSpeedRightLeft(length, column, row)
    length = length or 10
    column = column or 1
    row    = row or 1.5
    
    local output = ''

    local indicatorCount = calculateIndicatorCount(length)

    for i = 1, indicatorCount, 1 do
        if (i == indicatorCount) then
            output =  '<' .. output
            goto continue
        end

        output = '=' .. output
        ::continue::
    end

    for i = indicatorCount + 1, length, 1 do
        output =  '.' .. output
    end

    better.echo(output, column, row)
end

function lib.showSpeedTopDown(length, column, row)
    length = length or 10
    column = column or 1
    row    = row or 1
    
    local indicatorCount = calculateIndicatorCount(length)

    for i = 1, indicatorCount, 1 do
        if (i == indicatorCount) then
            better.echo('V', column, row + i - 1)
            goto continue
        end

        better.echo('I', column, row + i - 1)
        ::continue::
    end

    for i = indicatorCount + 1, length, 1 do
        better.echo('.', column, i)
    end
end

function lib.showSpeedBottomUp(length, column, row)
    length = length or 10
    column = column or 1
    row    = row or 1
    
    local indicatorCount = calculateIndicatorCount(length)

    for i = 1, indicatorCount, 1 do
        if (i == indicatorCount) then
            better.echo('A', column, length - row - i + 2)
            goto continue
        end

        better.echo('I', column, length - row - i + 2)
        ::continue::
    end

    for i = indicatorCount + 1, length, 1 do
        better.echo('.', column, length - i + row)
    end
end

function lib.onTick(length, column, row)
    
    showSpeed(length, column, row)
end

return lib
