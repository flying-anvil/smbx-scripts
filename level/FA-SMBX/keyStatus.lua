local lib = {}

local keysDown = {
    KEY_UP = false,
    KEY_DOWN = false,
    KEY_LEFT = false,
    KEY_RIGHT = false,
    KEY_JUMP = false,
    KEY_SPINJUMP = false,
    KEY_X = false,
    KEY_RUN = false,
    KEY_SEL = false,
    KEY_STR = false
}

function lib.isDown(keyCode)
    return keysDown[keyCode] == true
end

function lib.onKeyDown(keyCode)
    keysDown[keyCode] = true
end

function lib.onKeyUp(keyCode)
    keysDown[keyCode] = false
end

return lib
