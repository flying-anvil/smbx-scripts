local lib = {}

function lib.echo(content, column, row)
    Text.print(content, 18 * column, 18 * row)
end

return lib
