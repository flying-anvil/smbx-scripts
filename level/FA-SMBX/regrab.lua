local lib = {}
local better = require('FA-SMBX/betterPrint')

local keyStatus = {}
local maxRegrabFallSpeed = 8
local isDebug = false

function regrab(actor)
    if (actor == nil) then return end

    local isFalling = actor.speedY > 0
    local isGrabbing = keyStatus.isDown(KEY_JUMP) or keyStatus.isDown(KEY_SPINJUMP)
    local isRegrabbing = isFalling and isGrabbing

    showDebugInfo(actor, isFalling, isGrabbing, isRegrabbing)

    if (isRegrabbing) then
        actor.speedY = math.min(actor.speedY - .1, maxRegrabFallSpeed)
    end
end

function showDebugInfo(actor, isFalling, isGrabbing, isRegrabbing)
    if (isDebug == true) then
        better.echo('Falling:  ' .. tostring(isFalling), 1, 3)
        better.echo('Grabbing: ' .. tostring(isGrabbing), 1, 4)
        better.echo('ReGrab:   ' .. tostring(isRegrabbing), 1, 5)
        better.echo('Fall-Speed: ' .. actor.speedY, 1, 6)
        -- better.echo('JUMP:     ' .. tostring(keyStatus.isDown(KEY_JUMP)), 1, 5)
        -- better.echo('SpinJUMP: ' .. tostring(keyStatus.isDown(KEY_SPINJUMP)), 1, 6)
    end
end

function lib.init(newKeyStatus, newMaxFallSpeed)
    newMaxFallSpeed = newMaxFallSpeed or maxRegrabFallSpeed

    keyStatus = newKeyStatus
    maxRegrabFallSpeed = newMaxFallSpeed
    -- better.echo(newKeyStatus.isDown(KEY_JUMP)])
end

function lib.onTick()
    regrab(player)
    regrab(player2)
end

function lib.toggleDebug()
    isDebug = not isDebug
end

return lib
