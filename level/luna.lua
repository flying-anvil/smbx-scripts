
local regrab = require('FA-SMBX/regrab')
local speedMeter = require('FA-SMBX/speedMeter')
local keyStatus = require('FA-SMBX/keyStatus')

regrab.init(keyStatus)
-- regrab.toggleDebug()

function onTick()
    regrab.onTick()
    speedMeter.showSpeedLeftRight()
    -- speedMeter.showSpeedRightLeft(6, 0, 32)
    -- speedMeter.showSpeedTopDown(6, 1, 1)
    -- speedMeter.showSpeedBottomUp(6, 1, 1)
end

function onKeyDown(keyCode)
    keyStatus.onKeyDown(keyCode)
end

function onKeyUp(keyCode)
    keyStatus.onKeyUp(keyCode)
end
